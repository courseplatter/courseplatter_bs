package coursePlatter_BS;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoursePlatterBsApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoursePlatterBsApplication.class, args);
	}
}
